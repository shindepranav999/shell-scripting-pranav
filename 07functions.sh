# #!/bin/bash

# #declaring a funtion 

# sample() 
# {

# echo "HI im a sample funtion"
# # to print error code from a log file -->
# # tail -n /logs/filename | grep "errorcode"
# }

# sample
# echo "calling sample again"
# sample

#!/bin/bash

stat()
{
    echo loadAverage = $(uptime | awk -F : '{print $NF}' | awk -F , '{print $1}')
    echo Number of users logged in = $(who | wc -l)

}
stat
# nested dictionary:-

employee = {
            101:{'name':'karan','age':21,'salary':30000},
            102:{'name':'jay','age':22,'salary':30000},
	    103:{'name':'jay','age':22,'salary':30000}
            }
emp_id=int(input("Enter the id for fetching data from data structure:"))
print(employee[102])
print("below are details for id:",emp_id)
for ele in employee[102]:
     print(f"{ele} ---> {employee[102][ele]}")
    


ex.
employee = {
            101:{'name':'karan','age':21,'salary':30000},
            102:{'name':'jay','age':[20,30,40],'salary':30000},
	        103:{'name':'jay','age':22,'salary':30000}
        }
#print(employee)
print(employee[102]['age'][1])  #22
amt=float(input("Enter the bonus amount"))
employee[102]['salary']=employee[102]['salary']+amt
print(employee)

module in python:-
- Any python file having .py extension.
python module
python script

module,script,library,package,framework etc

how to import things from module.

main.py
import add,subtract,multiply,div
num1=float(input("Enter the first number:"))
num2=float(input("Enter the second number:"))
print(add.addition(num1,num2))
print(subtract.sub(num1,num2))

add.py
def addition(num1,num2):
    return num1+num2

subtract.py
def sub(num1,num2):
    return num1-num2

multiply.py
def mul(num1,num2):
    return num1*num2

div.py
def division(num1,num2):
    return num1/num2

#importing entire module

#import module by renaming
import add as a
num1=float(input("Enter the first number:"))
num2=float(input("Enter the second number:"))
print(a.addition(num1,num2))


#import particular function from another module.
from add import addition
num1=float(input("Enter the first number:"))
num2=float(input("Enter the second number:"))
print(addition(num1,num2))


#import multiple functions from anothe rmodule
from add import addition,display
num1=float(input("Enter the first number:"))
num2=float(input("Enter the second number:"))
print(addition(num1,num2))


#import everything from module.
from add import *
num1=float(input("Enter the first number:"))
num2=float(input("Enter the second number:"))
print(addition(num1,num2))
print(display())


module is reloaded only once.























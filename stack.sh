#!/bin/bash


ID=$(id -u)
if [ $ID -ne 0 ] ; then
    echo -e "YOu should br root user for runnning this script or run with the sudo command"
    exit 1
fi

stat() {
    if [ $? -eq 0 ] ; then
        echo -e "\e[32m Success \e[0m"
    else
        echo -e "\e[32m Failure \e[0m"
    fi
}

LOG=/tmp/stack.log
FUSER=student
VERSION=8.5.73
APACHE_URL=https://dlcdn.apache.org/tomcat/tomcat-8/v$VERSION/bin/apache-tomcat-$VERSION.tar.gz
WAR_URL="https://devops-cloudcareers.s3.ap-south-1.amazonaws.com/student.war"
JDBC_URL="https://devops-cloudcareers.s3.ap-south-1.amazonaws.com/mysql-connector.jar "


#https://dlcdn.apache.org/tomcat/tomcat-8/v8.5.73/bin/apache-tomcat-8.5.73.tar.gz
# 1 -> Webserver (front-end)
# installation of Apache webserver


echo -n "Installing Apache webserver : "
yum install httpd -y &> $LOG
stat $?

#  Proxy /reverse prxy entries

echo -n "Creating PRoxy config : "
echo 'ProxyPass "/student" "http://localhost:8080/student"
ProxyPassReverse "/student"  "http://localhost:8080/student" ' > /etc/httpd/conf.d/proxy.conf 
stat $?

# Setup default index file

echo -n "Setup default index file : "
curl -s https://devops-cloudcareers.s3.ap-south-1.amazonaws.com/index.html > /var/www/html/index.html
stat $?

#starting webservices
echo -n "Start web-service : "
systemctl enable httpd &> $LOG
systemctl start httpd &> $LOG
stat $?

# END OF WEBSERVER (Front-end)
# ----------------------------------------------------------------------------------------------------------------------------------

# 2 -> Appserver (Back-end)
# setting up Appserver

echo -n "Installing java : "
sudo yum install java -y &> $LOG
stat $?

echo -n "Creating $FUSER functional user : "
id $FUSER &> LOG
if [ $? -eq 0 ] ; then
    echo -n "SKIPPING, $FUSER already exists"
else
    useradd $FUSER &> $LOG
    stat $?
fi

echo -n "Downloading the Tomcat : "
cd /home/$FUSER 
wget $APACHE_URL  &> $LOG 
tar -xf apache-tomcat-$VERSION.tar.gz  &> $LOG 
stat $?

echo -n "Downloading the $FUSER War file : "
wget $WAR_URL -P apache-tomcat-$VERSION/webapps  &> $LOG 
stat $?

echo -n "Downloading the JDBC : "
wget $JDBC_URL -P apache-tomcat-$VERSION/lib  &> $LOG 
chown -R $FUSER:$FUSER apache-tomcat-$VERSION  &> $LOG  
stat $?

echo -n "Starting the TOMCAT"
sh apache-tomcat-$VERSION/bin/startup.sh   &> $LOG 
sleep 5 &> $LOG
stat $? 

echo -n "Checking App's Availability : " 
curl localhost:8080/$FUSER 
if [ $? -eq 0 ]; then 
    echo -e "\e[32m Application is UP and Ready to serve the traffic \e[0m"
else 
    echo -e "\e[31m App is having issues. Refer to the logs in /home/$FUSER/apache-tomcat-$VERSION/logs/catalina.out \e[0m"
fi

#!/bin/bash

### Special Variables are $0 , $n , $@, $# ,$* , $? ( gives you the exit code of the last executed cmd)

echo $0   ### Prints the script name 
echo "I am pritning first variable from cmmand line $1"   # This will be the first argument from the command line while executing
echo  "I am pritning second variable from cmmand line $2"    # This will be the second argument from the command line while executing

echo Supplied variables are $*  # Prints the supplied variables
echo Number of supplied variables ara $#   # Number of variables

# sh scriptName variValue1 varValue2
#                   $1         $2
